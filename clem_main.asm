;
; Mitch Clem
; 12/5/16
; CSC-330 Final Project
; Monophonic Synthesizer code: 8-bit ADC to set frequency, 8-bit DAC for output, button for shape
;


.def				reg_workhorse	= r16			; used for all general register operations
.def				amplitude		= r17
.cseg													
.org				0x0000							; reset vector
					rjmp	setup
.org				0x0020							; ISR vector setup (vectors are interrupt specific)
					rjmp	ISR_OVF0
					0x0100							; start of non-reserved program memory

setup:				ser		reg_workhorse					; set all bits in register
					out		DDRD, reg_workhorse				; all pins in port D set to outputs
					ldi		reg_workhorse, 0b00000000
					out		TCCR0A, reg_workhorse			; initialized the timer
					ldi		reg_workhorse, 0b00000100		; sets the timer prescalar (256)
					out		TCCR0B, reg_workhorse
					ldi		reg_workhorse, 0b00000001
					sts		TIMSK0, reg_workhorse			; enables local timer interrupt
					sei										; enables global interrupt (bit 7 of the status register)

					; outputs counter value
loop:				nop
					out		PORTD, amplitude
					rjmp	loop

					; interrupt routine that pushes workhorse to the stack,
					; incriments the counter, and pops from the stack
ISR_OVF0:			push	reg_workhorse
					in		reg_workhorse, SREG
					push	reg_workhorse
					inc		amplitude					; incriments the counter
					pop		reg_workhorse
					out		SREG, reg_workhorse
					pop		reg_workhorse
					reti								; return from interrupt



													
													