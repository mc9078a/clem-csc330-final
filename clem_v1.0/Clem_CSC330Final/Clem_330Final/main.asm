; Mitch Clem
; CSC-330 Final Project
; 13 December 2016
; Monophonic synthesizer
;
; Author : mc9078a


;define constants
.def                reg_workhorse   = r16                       ; used for general register operations
.def                amplitude       = r17
.def                adc_val         = r18
.def                sreg_temp       = r19
.def                dir             = r20
.def                go_flag         = r21
.def                table_count     = r22
.def                tri_count       = r23


.cseg
; vector setup												
.org                0x0000                                      ; reset vector
                    rjmp        setup
.org                0x001C                                      ; ISR OVERFLOW vector setup
                    rjmp        ISR_OVF
.org                0x002A                                      ; ADC ISR vector setup
                    rjmp        ISR_ADC
.org                0x0100                                      ; start of non-reserved program memory


; general setup and initialization
setup:              ser         reg_workhorse                   ; set all bits in register
                    out         DDRD, reg_workhorse             ; all pins in port D set to outputs
                    ldi         amplitude, 0b00000000           ;initialized amplitude to 0
                    ldi         dir, 0b11111111                 ;initializes triangle wave direction to up
                    ldi         tri_count, 0b00000000                                               
                    ldi         reg_workhorse, 0b00000010		
                    out         TCCR0A, reg_workhorse			
                    ldi         reg_workhorse, 0b00000011       ; sets timer prescalar (64)
                    out         TCCR0B, reg_workhorse
                    ldi         reg_workhorse, 0b00000010		
                    sts         TIMSK0, reg_workhorse           ; enables local timer interrupt					

                    ; initialize ADC1
                    ldi         reg_workhorse, 0b01100101
                    sts         ADMUX, reg_workhorse
                    ldi         reg_workhorse, 0b11101111
                    sts         ADCSRA, reg_workhorse

                    ; initialize stack pointer
                    ldi         ZL, low(RAMEND)
                    out         SPL, ZL
                    ldi         ZH, high(RAMEND)
                    out         SPH, ZH

                    ; initialize sine table directive with pointers
                    ldi         ZL, low(sine_table*2)
                    ldi         ZH, high(sine_table*2)

                    sei
			
; wave selection, construction, and output loop														
loop:		
;wavetype selection: comment out the two wave types not in use
                    rjmp sawtooth		
                    ;rjmp triangle
                    ;rjmp sine

;-------sawtooth wave routine set--------------------------------------------------------------------------------------------------------
        sawtooth:       cpi        go_flag, 0b11111111
                        breq       sawreset
                        rjmp       loop
		
        sawreset:       ldi        go_flag, 0b00000000
					
        ;outputs and increments amplitude
        sup:            out        PORTD, amplitude
                        inc        amplitude
                        rjmp       loop
					
		

;-------triangle wave routine set---------------------------------------------------------------------------------------------------------
        triangle:       cpi         go_flag, 0b11111111
                        breq        trireset				
                        rjmp        loop

        trireset:       ldi         go_flag, 0b00000000        ;resets wave activation flag
                        cpi         dir, 0b11111111
                        breq        up
                        cpi         dir, 0b00000000
                        breq        down
                    
                    

		;checks to make sure amplitude is not at the top of wave cycle, outputs and increments
        up:             cpi         amplitude, 0b11111111
                        breq        dir_set_down
                        out         PORTD, amplitude
                        inc         amplitude
                        rjmp        loop

        ;checks to make sure amplitude is not at the bottom of wave cycle, outputs and decrements
        down:           cpi         amplitude, 0b00000000
                        breq        dir_set_up
                        out         PORTD, amplitude
                        dec         amplitude
                        rjmp        loop

        ;sets the dir register that tells the triangle wave whether to inc or dec     
        dir_set_up:     ldi         dir, 0b11111111
                        rjmp        loop
        dir_set_down:   ldi         dir, 0b00000000
                        rjmp        loop
        

;-------sine wave routine set------------------------------------------------------------------------------------------------------------
        sine:           cpi         go_flag, 0b11111111
                        breq        sinereset				
                        rjmp        loop	

        sinereset:      ldi         go_flag, 0b00000000          ;resets wave activation flag

		;ouputs sine wave table values and increments pointer
        smooth:         lpm         amplitude, Z+             				
                        out         PORTD, amplitude            
                        inc         table_count                  ;increments array count
                        cpi         table_count, 0b11111111
                        breq        table_reset                  ;breaks to table_reset if pointer reaches end of sine lookup table
                        rjmp        loop

		; resets the sine table array counter and reinitializes the pointers, jumps back to smooth routine
        table_reset:    ldi         table_count, 0b00000000
                        ldi         ZL, low(sine_table*2)
                        ldi         ZH, high(sine_table*2)
                        rjmp        loop




;overflow code section--------------------------------------------------------------------------------------------------------------------------------

; overflow interrupt routine that pushes workhorse to the stack, sets the update flag, and pops from the stack
ISR_OVF:            push        reg_workhorse
                    in          reg_workhorse, SREG
                    push        reg_workhorse

                    ldi         go_flag, 0b11111111     ; sets the wave activation flag

                    pop         reg_workhorse
                    out         SREG, reg_workhorse
                    pop         reg_workhorse
                    reti                                ; return from interrupt



; interrupt routine that takes the potentiometer input and uses output compare to control the timer length and f of wave 
ISR_ADC:            in          sreg_temp, SREG
                    push        sreg_temp
                    lds         adc_val, ADCH
                    out         OCR0A, adc_val		
                    pop         sreg_temp
                    out         SREG, sreg_temp
                    reti

;sine wave look up table generated on the internet----------------------------------------------------------------------------------------------------------------------------------------------------------------------
sine_table:         .db         128,131,134,137,140,143,146,149,152,155,158,162,165,167,170,173,176,179,182,185,188,190,193,196,198,201,203,206,208,211,213,215,218,220,222,224,226,228,230,232,234,235,237,238,240,241,243,244,245,246,248,249,250,250,251,252,253,253,254,254,254,255,255,255,255,255,255,255,254,254,254,253,253,252,251,250,250,249,248,246,245,244,243,241,240,238,237,235,234,232,230,228,226,224,222,220,218,215,213,211,208,206,203,201,198,196,193,190,188,185,182,179,176,173,170,167,165,162,158,155,152,149,146,143,140,137,134,131,128,124,121,118,115,112,109,106,103,100,97,93,90,88,85,82,79,76,73,70,67,65,62,59,57,54,52,49,47,44,42,40,37,35,33,31,29,27,25,23,21,20,18,17,15,14,12,11,10,9,7,6,5,5,4,3,2,2,1,1,1,0,0,0,0,0,0,0,1,1,1,2,2,3,4,5,5,6,7,9,10,11,12,14,15,17,18,20,21,23,25,27,29,31,33,35,37,40,42,44,47,49,52,54,57,59,62,65,67,70,73,76,79,82,85,88,90,93,97,100,103,106,109,112,115,118,121,124





